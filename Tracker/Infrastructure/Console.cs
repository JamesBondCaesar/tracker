using ConsoleTables;
using Tracker.Extensions;
using Tracker.Models;

namespace Tracker.Infrastructure;

public static class Console
{
    public static void PrintTable(StatisticData statistic)
    {
        var tableData = statistic.ToTableData();

        var table = new ConsoleTable(tableData.Data.Select(x => x.Header).ToArray())
            .Configure(o => o.NumberAlignment = Alignment.Right);

        for (int i = 0; i < tableData.TotalRows; i++)
            table.AddRow(tableData.Data.Select(x => x.Data[i]).ToArray<object>());

        table.Write();
    }

    public static void PrintTable<T>(IEnumerable<T> collection)
    {
        ConsoleTable.From(collection)
            .Configure(o => o.NumberAlignment = Alignment.Right)
            .Write();
    }
}