using Tracker.Models;

namespace Tracker.Infrastructure;

public static class Statistic
{
    public static StatisticData Calculate(
        IReadOnlyCollection<DataAccess.Models.Task>? tasks,
        DateTimeOffset? from = null,
        DateTimeOffset? to = null)
    {
        if (from.HasValue && to.HasValue && from.Value >= to.Value)
            throw new Exception("'From date' must be less or equal to 'To date'");

        if (tasks == null || !tasks.Any())
        {
            return new StatisticData
            {
                StatisticByTasks = Array.Empty<StatisticItem>(),
                TotalTimeByDays = new Dictionary<DateTime, TimeSpan>()
            };
        }

        var tasksWithStatistic = tasks.Where(x => x.Statistics != null && x.Statistics.Any())
            .OrderBy(x => x.Statistics!.Select(y => y.StartTime).Min());

        var statisticByTasks = new List<StatisticItem>();
        var totalTimeByDays = new Dictionary<DateTime, TimeSpan>();

        foreach (var task in tasksWithStatistic)
        {
            var statisticItem = CalculateStatisticForTask(task, from, to);

            statisticByTasks.Add(statisticItem);

            foreach (var day in statisticItem.HoursByDays.Keys)
            {
                if (totalTimeByDays.TryGetValue(day, out var total))
                {
                    totalTimeByDays[day] = total + statisticItem.HoursByDays[day];
                }
                else
                {
                    totalTimeByDays[day] = statisticItem.HoursByDays[day];
                }
            }
        }

        return new StatisticData
        {
            StatisticByTasks = statisticByTasks.Any()
                ? statisticByTasks.OrderBy(x => x.Task.Project.Name).ThenBy(x => x.Task.Name).ToList()
                : statisticByTasks,
            TotalTimeByDays = totalTimeByDays.Any()
                ? totalTimeByDays.OrderBy(x => x.Key).ToDictionary(x => x.Key, x => x.Value)
                : totalTimeByDays,
            From = totalTimeByDays.Any() ? totalTimeByDays.Keys.Min() : DateTimeOffset.MinValue,
            To = totalTimeByDays.Any() ? totalTimeByDays.Keys.Max() : DateTimeOffset.MinValue
        };
    }

    private static StatisticItem CalculateStatisticForTask(DataAccess.Models.Task task, 
	DateTimeOffset? from,
        DateTimeOffset? to)
    {
        var statisticResult = GetDailyStatistic(task, from, to);
        var hoursByDays = statisticResult.GroupBy(x => x.Date)
            .ToDictionary(x => x.Key, x => TimeSpan.FromHours(x.Sum(y => y.Hour)));
        var totalHours = TimeSpan.FromHours(statisticResult.Sum(x => x.Hour));

        return new StatisticItem(task, totalHours, hoursByDays);
    }

    private static IReadOnlyCollection<DailyStatisticInfo> GetDailyStatistic(DataAccess.Models.Task task,
        DateTimeOffset? from, DateTimeOffset? to)
    {
        var statisticItems = task.Statistics!.OrderBy(x => x.StartTime);
        var res = new List<DailyStatisticInfo>();

        foreach (var statistic in statisticItems)
        {
            var endTime = (statistic.FinishTime ?? DateTimeOffset.UtcNow);
            var startTime = statistic.StartTime;

            while (startTime < endTime)
            {
                var toDate = startTime.Date < endTime.Date
                    ? startTime.Date.AddDays(1)
                    : endTime;

                if (from.HasValue && startTime < from.Value)
                {
                    startTime = toDate;
                    continue;
                }

                if (to.HasValue && startTime > to.Value)
                {
                    break;
                }

                res.Add(new DailyStatisticInfo(startTime.Date, (toDate - startTime).TotalHours));

                startTime = toDate;
            }
        }

        return res;
    }

    record DailyStatisticInfo(DateTime Date, double Hour);
}
