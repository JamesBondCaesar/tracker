using Microsoft.EntityFrameworkCore;
using Tracker.DataAccess;
using Tracker.DataAccess.Models;
using Task = System.Threading.Tasks.Task;

namespace Tracker.Infrastructure;

public class ProjectManager : IProjectManager
{
    private readonly DatabaseContext _context;

    public ProjectManager(DatabaseContext context)
    {
        _context = context;
    }

    public async Task Create(string projectName, CancellationToken cancellationToken)
    {
        if (await _context.Projects.AnyAsync(x => x.Name == projectName, cancellationToken))
            throw new Exception("Project already existis");

        await _context.Projects.AddAsync(new Project
        {
            Name = projectName
        }, cancellationToken);
        await _context.SaveChangesAsync(cancellationToken);
    }

    public Task Delete(string projectName, CancellationToken cancellationToken) =>
        _context.Projects.Where(x => x.Name == projectName).ExecuteDeleteAsync(cancellationToken);

    public async Task<IReadOnlyCollection<Project>> Get(CancellationToken cancellationToken) =>
        await _context.Projects
            .Include(x=>x.Tasks)
            .ThenInclude(x=>x.Statistics)
            .ToListAsync(cancellationToken);
}