using Tracker.Extensions;
using Tracker.Models;

namespace Tracker.Infrastructure;

public static class File
{
    private const char Separator = ';';

    public static void Export(string fileName, StatisticData statistic)
    {
        if (System.IO.File.Exists(fileName))
            System.IO.File.Delete(fileName);

        var tableData = statistic.ToTableData();

        var fileContent = new List<string>();
        fileContent.Add(string.Join(Separator, tableData.Data.Select(x => x.Header)));

        for (int i = 0; i < tableData.TotalRows; i++)
            fileContent.Add(string.Join(Separator, tableData.Data.Select(x => x.Data[i])));

        System.IO.File.AppendAllLines(fileName, fileContent);
    }
}