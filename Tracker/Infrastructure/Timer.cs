using Microsoft.EntityFrameworkCore;
using Tracker.DataAccess;
using Tracker.DataAccess.Models;
using Task = System.Threading.Tasks.Task;

namespace Tracker.Infrastructure;

public class Timer : ITimer
{
    private readonly DatabaseContext _context;

    public Timer(DatabaseContext context)
    {
        _context = context;
    }

    public async Task Start(string taskName, CancellationToken cancellationToken = default)
    {
        var task = await GetTask(taskName, cancellationToken);

        if (!task.IsActive)
            throw new Exception("Task is not active");

        if (task.Statistics == null || !task.Statistics.Any() || task.Statistics.All(x => x.FinishTime.HasValue))
        {
            await _context.Statistics.AddAsync(new DataAccess.Models.Statistic
            {
                Task = task,
                StartTime = DateTimeOffset.UtcNow
            }, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);
        }
    }

    public async Task Stop(string taskName, CancellationToken cancellationToken = default)
    {
        var task = await GetTask(taskName, cancellationToken);

        if (!task.IsActive)
            throw new Exception("Task is not active");

        var activeStatistic = task.Statistics?.Where(x => !x.FinishTime.HasValue);

        if (activeStatistic == null) return;

        foreach (var activeTask in activeStatistic)
        {
            activeTask.FinishTime = DateTimeOffset.UtcNow;
        }

        await _context.SaveChangesAsync(cancellationToken);
    }

    public async Task<IReadOnlyCollection<DataAccess.Models.Task>> TrackingTasks(CancellationToken cancellationToken = default) =>
        await _context.Tasks.Where(x => x.Statistics!.Any(y => !y.FinishTime.HasValue))
            .Include(x => x.Project)
            .Include(x=>x.Statistics)
            .ToListAsync(cancellationToken);

    public async Task<IReadOnlyCollection<Project>> TrackingProjects(CancellationToken cancellationToken = default) =>
        await _context.Projects.Where(x => x.Tasks.Any(t => t.Statistics!.Any(s => !s.FinishTime.HasValue)))
            .Include(x => x.Tasks)
            .ToListAsync(cancellationToken);

    private async Task<DataAccess.Models.Task> GetTask(string taskName, CancellationToken cancellationToken)
    {
        var task = await _context.Tasks
            .Include(x => x.Statistics)
            .FirstOrDefaultAsync(x => x.Name == taskName, cancellationToken);

        if (task == null)
            throw new Exception($"Task '{taskName}' not found");
        return task;
    }
}