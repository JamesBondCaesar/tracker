using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Tracker.DataAccess;

namespace Tracker.Infrastructure;

public class TaskManager : ITaskManager
{
    private readonly DatabaseContext _context;

    public TaskManager(DatabaseContext context)
    {
        _context = context;
    }

    public async Task CreateTask(string projectName, string taskName, CancellationToken cancellationToken = default)
    {
        var project = await _context.Projects.FirstOrDefaultAsync(x => x.Name == projectName, cancellationToken);
        if (project == null)
            throw new Exception("Project not found");

        if (await _context.Tasks.AnyAsync(x => x.Name == taskName, cancellationToken))
            throw new Exception("Task already exists");

        await _context.Tasks.AddAsync(new DataAccess.Models.Task
        {
            Name = taskName,
            Project = project,
            CreateDate = DateTimeOffset.UtcNow,
            IsActive = true
        }, cancellationToken);

        await _context.SaveChangesAsync(cancellationToken);
    }

    public async Task Delete(string taskName, CancellationToken cancellationToken = default)
    {
        var task = await _context.Tasks
            .Include(x => x.Statistics)
            .FirstOrDefaultAsync(x => x.Name == taskName, cancellationToken);

        if (task != null && (task.Statistics == null || !task.Statistics.Any()))
        {
            _context.Tasks.Remove(task);
            await _context.SaveChangesAsync(cancellationToken);
        }
        else
        {
            throw new Exception("Task not found or already has tracking statistic");
        }
    }

    private IQueryable<DataAccess.Models.Task> GetTasks(
        string[]? projects,
        DateTimeOffset? from,
        DateTimeOffset? to,
        Expression<Func<DataAccess.Models.Task, bool>>? predicate = null)
    {
        if (from.HasValue && to.HasValue && from.Value >= to.Value)
            throw new Exception("'From date' must be less or equal to 'To date'");

        IQueryable<DataAccess.Models.Task> query = _context.Tasks
            .Include(x => x.Statistics)
            .Include(x => x.Project);

        if (from.HasValue)
            query = query.Where(x => x.CreateDate >= from.Value.UtcDateTime);
        if (to.HasValue)
            query = query.Where(x => x.CreateDate <= to.Value.UtcDateTime);
        if (projects is { Length: > 0 })
            query = query.Where(x => projects.Contains(x.Project.Name));
        if (predicate != null)
            query = query.Where(predicate);

        return query;
    }

    public async Task<IReadOnlyCollection<DataAccess.Models.Task>> GetAll(
        string[]? projects,
        DateTimeOffset? from,
        DateTimeOffset? to,
        CancellationToken cancellationToken = default) =>
        await GetTasks(projects, from, to).ToListAsync(cancellationToken);

    public async Task<IReadOnlyCollection<DataAccess.Models.Task>> GetActive(
        string[]? projects,
        DateTimeOffset? from,
        DateTimeOffset? to,
        CancellationToken cancellationToken = default) =>
        await GetTasks(projects, from, to, x => x.IsActive).ToListAsync(cancellationToken);

    public Task<DataAccess.Models.Task> Get(string taskName, CancellationToken cancellationToken = default) =>
        _context.Tasks.Where(x => x.Name == taskName)
            .Include(x => x.Project)
            .Include(x => x.Statistics)
            .FirstAsync(cancellationToken);

    public async Task Activate(string taskName, CancellationToken cancellationToken = default)
    {
        var task = await _context.Tasks.FirstAsync(x => x.Name == taskName, cancellationToken);
        task.IsActive = true;
        task.FinishDate = null;

        await _context.SaveChangesAsync(cancellationToken);
    }

    public async Task Deactivate(string taskName, CancellationToken cancellationToken = default)
    {
        var task = await _context.Tasks
            .Include(x => x.Statistics)
            .FirstAsync(x => x.Name == taskName, cancellationToken);

        task.IsActive = false;
        task.FinishDate = DateTimeOffset.UtcNow;

        var trackedStatistic = task.Statistics?.Where(x => !x.FinishTime.HasValue);

        if (trackedStatistic != null)
        {
            foreach (var taskStatistic in trackedStatistic)
            {
                taskStatistic.FinishTime = DateTimeOffset.UtcNow;
            }
        }

        await _context.SaveChangesAsync(cancellationToken);
    }
}