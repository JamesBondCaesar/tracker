namespace Tracker.Infrastructure;

public interface ITaskManager
{
    public Task CreateTask(string projectName, string taskName, CancellationToken cancellationToken = default);
    public Task Delete(string taskName, CancellationToken cancellationToken = default);

    public Task<IReadOnlyCollection<DataAccess.Models.Task>> GetAll(string[]? projects,
        DateTimeOffset? from,
        DateTimeOffset? to,
        CancellationToken cancellationToken = default);

    public Task<IReadOnlyCollection<DataAccess.Models.Task>> GetActive(string[]? projects,
        DateTimeOffset? from,
        DateTimeOffset? to,
        CancellationToken cancellationToken = default);

    public Task<DataAccess.Models.Task> Get(string taskName, CancellationToken cancellationToken = default);
    public Task Activate(string taskName, CancellationToken cancellationToken = default);
    public Task Deactivate(string taskName, CancellationToken cancellationToken = default);
}