using Task = System.Threading.Tasks.Task;

namespace Tracker.Infrastructure;

public interface ITimer
{
    public Task Start(string taskName, CancellationToken cancellationToken = default);
    public Task Stop(string taskName, CancellationToken cancellationToken = default);
    public Task<IReadOnlyCollection<DataAccess.Models.Task>> TrackingTasks(CancellationToken cancellationToken = default);
    public Task<IReadOnlyCollection<DataAccess.Models.Project>> TrackingProjects(CancellationToken cancellationToken = default);
}