namespace Tracker.DataAccess.Models;

public class Statistic
{
    public Guid Id { get; set; }
    public DateTimeOffset StartTime { get; set; }
    public DateTimeOffset? FinishTime { get; set; }
    public Task Task { get; set; } = default!;
    public Guid TaskId { get; set; }
}