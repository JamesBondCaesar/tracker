namespace Tracker.DataAccess.Models;

public class Project
{
    public Guid Id { get; set; }
    public string Name { get; set; } = default!;
    public ICollection<Task> Tasks { get; } = default!;
}