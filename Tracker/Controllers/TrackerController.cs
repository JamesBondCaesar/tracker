using Tracker.Infrastructure;
using Tracker.Models;
using Console = Tracker.Infrastructure.Console;

namespace Tracker.Controllers;

public class TrackerController : ConsoleAppBase
{
    private readonly ITimer _timer;

    public TrackerController(ITimer timer)
    {
        _timer = timer;
    }

    [Command("start", "Delete task")]
    public Task Start([Option(0, "Task name")] string taskName) => _timer.Start(taskName);

    [Command("stop", "Delete task")]
    public Task Stop([Option(0, "Task name")] string taskName) => _timer.Stop(taskName);

    [Command("list", "List of tracking tasks")]
    public async Task List()
    {
        var tasks = (await _timer.TrackingTasks())
            .Select(x => new TaskInfo
            {
                Id = x.Id,
                Name = x.Name,
                CreateDate = x.CreateDate,
                IsActive = x.IsActive,
                ProjectName = x.Project.Name,
                Active = TimeSpan.FromTicks(x.Statistics!.Where(s => !s.FinishTime.HasValue)
                    .Sum(s => (DateTimeOffset.UtcNow - s.StartTime).Duration().Ticks)),
                Total = TimeSpan.FromTicks(x.Statistics!.Sum(s =>
                    ((s.FinishTime ?? DateTimeOffset.UtcNow) - s.StartTime).Duration().Ticks))
            });
        Console.PrintTable(tasks);
    }
}