using Tracker.Enums;
using Tracker.Infrastructure;
using Tracker.Models;
using Console = Tracker.Infrastructure.Console;

namespace Tracker.Controllers;

[Command("task", "Tasks managr")]
public class TaskController : ConsoleAppBase
{
    private readonly ITaskManager _taskManager;

    public TaskController(ITaskManager taskManager)
    {
        _taskManager = taskManager;
    }

    [Command("add", "Add new task")]
    public Task Create([Option(0, "Project name")] string projectName, [Option(1, "Task name")] string taskName) =>
        _taskManager.CreateTask(projectName, taskName);

    [Command("delete", "Delete task")]
    public Task Delete([Option(0, "Task name")] string taskName) =>
        _taskManager.Delete(taskName);

    [Command("list", "Tasks list")]
    public async Task List([Option("t", "Active / All")] TaskType type = TaskType.Active,
        [Option("p", "projects")] string[]? projects = null,
        DateTimeOffset? from = null,
        DateTimeOffset? to = null)
    {
        var tasks = (type == TaskType.Active
                ? await _taskManager.GetActive(projects, from, to)
                : await _taskManager.GetAll(projects, from, to))
            .Select(x => new TaskInfo
            {
                Id = x.Id,
                Name = x.Name,
                CreateDate = x.CreateDate,
                IsActive = x.IsActive,
                ProjectName = x.Project.Name,
                Active = TimeSpan.FromTicks(x.Statistics!.Where(s => !s.FinishTime.HasValue)
                    .Sum(s => (DateTimeOffset.UtcNow - s.StartTime).Duration().Ticks)),
                Total = TimeSpan.FromTicks(x.Statistics!.Sum(s =>
                    ((s.FinishTime ?? DateTimeOffset.UtcNow) - s.StartTime).Duration().Ticks))
            });

        Console.PrintTable(tasks);
    }

    [Command("activate", "Activate task")]
    public Task Activate([Option(0, "Task name")] string taskName) => _taskManager.Activate(taskName);

    [Command("deactivate", "Deactivate task")]
    public Task Deactivate([Option(0, "Task name")] string taskName) => _taskManager.Deactivate(taskName);
}