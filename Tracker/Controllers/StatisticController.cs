using Tracker.Enums;
using Tracker.Infrastructure;
using Console = Tracker.Infrastructure.Console;

namespace Tracker.Controllers;

[Command("statistic", "Calculate statistic")]
public class StatisticController : ConsoleAppBase
{
    private readonly ITaskManager _taskManager;

    public StatisticController(ITaskManager taskManager)
    {
        _taskManager = taskManager;
    }

    [Command("show", "Show statistic in console")]
    public async Task Show([
            Option("t", "Active / All")]
        TaskType type = TaskType.All,
        [Option("p", "projects")] string[]? projects = null,
        DateTimeOffset? from = null,
        DateTimeOffset? to = null)
    {
        var tasks = type == TaskType.All
            ? await _taskManager.GetAll(projects, null, null)
            : await _taskManager.GetActive(projects, null, null);

        var statistic = Statistic.Calculate(tasks, from, to);

        Console.PrintTable(statistic);
    }

    [Command("export", "Show statistic in console")]
    public async Task Export(
        [Option("p", "path-to-file")] string path,
        [Option("t", "Active / All")] TaskType type = TaskType.All,
        [Option("p", "projects")] string[]? projects = null,
        DateTimeOffset? from = null,
        DateTimeOffset? to = null)
    {
        var tasks = type == TaskType.All
            ? await _taskManager.GetAll(projects, null, null)
            : await _taskManager.GetActive(projects, null, null);

        var statistic = Statistic.Calculate(tasks, from, to);

        Tracker.Infrastructure.File.Export(path, statistic);
    }
}