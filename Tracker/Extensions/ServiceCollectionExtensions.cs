using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Tracker.DataAccess;

namespace Tracker.Extensions;

public static class ServiceCollectionExtensions
{
    /// <summary>
    /// Добавление контекста базы данных.
    /// </summary>
    public static IServiceCollection AddDatabaseContext(this IServiceCollection services)
    {
        return services.AddDbContext<DatabaseContext>((sp, opt) =>
        {
            var configuration = sp.GetRequiredService<IConfiguration>();

            var connectionString = configuration.GetConnectionString(nameof(DatabaseContext));
            ArgumentNullException.ThrowIfNull(configuration);

            opt.UseNpgsql(connectionString);
        });
    }
}