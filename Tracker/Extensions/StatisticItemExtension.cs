using Tracker.Models;

namespace Tracker.Extensions;

public static class StatisticItemExtension
{
    private const string ProjectColumnName = "Project";
    private const string TaskColumnName = "Task";
    private const string TotalName = "Total";
    private const string DateFormat = "dd.MM.yyyy";

    public static TableData ToTableData(this StatisticData statistic)
    {
        var res = new List<TableColumnData>
        {
            new()
            {
                Header = ProjectColumnName,
                Data = statistic.StatisticByTasks.Select(t => t.Task.Project.Name).ToList()
            },
            new()
            {
                Header = TaskColumnName,
                Data = statistic.StatisticByTasks.Select(t => t.Task.Name).ToList()
            }
        };
        res.AddRange(statistic.TotalTimeByDays.Keys.Select(day => new TableColumnData
        {
            Header = day.ToString(DateFormat),
            Data = new List<string>()
        }));
        res.Add(new TableColumnData
        {
            Header = TotalName,
            Data = new List<string>()
        });

        foreach (var task in statistic.StatisticByTasks)
        {
            var usedTime = statistic.TotalTimeByDays.Keys.Select(dt => task.HoursByDays.TryGetValue(dt, out var hour)
                    ? hour
                    : TimeSpan.Zero)
                .ToArray();

            var totalByTask = TimeSpan.Zero;

            for (var i = 2; i < res.Count - 1; i++)
            {
                var time = usedTime[i - 2];
                totalByTask = totalByTask.Add(time);
                res[i].Data.Add(time == TimeSpan.Zero ? string.Empty : time.ToString("g"));
            }

            res[^1].Data.Add(totalByTask.ToString("g"));
        }

        var totalHours = TimeSpan.Zero;

        res[0].Data.Add(TotalName);
        res[1].Data.Add(string.Empty);
        {
            var ind = 2;
            foreach (var dailyHour in statistic.TotalTimeByDays.Values)
            {
                totalHours = totalHours.Add(dailyHour);
                res[ind++].Data.Add(dailyHour.ToString("g"));
            }
        }
        res[^1].Data.Add($"({totalHours})");

        return new TableData
        {
            Data = res,
            TotalRows = statistic.StatisticByTasks.Count + 1
        };
    }
}