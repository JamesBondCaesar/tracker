namespace Tracker.Models;

public record TableColumnData
{
    public string Header { get; init; } = default!;
    public List<string> Data { get; init; } = default!;
}