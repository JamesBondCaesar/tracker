namespace Tracker.Models;

public record TableData
{
    public int TotalRows { get; init; }
    public IReadOnlyCollection<TableColumnData> Data { get; init; } = default!;
}