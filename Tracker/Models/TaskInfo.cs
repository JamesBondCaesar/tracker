namespace Tracker.Models;

public record TaskInfo
{
    public Guid Id { get; init; }
    public string Name { get; init; } = default!;
    public bool IsActive { get; init; }

    public DateTimeOffset CreateDate { get; init; }

    public string ProjectName { get; init; } = default!;

    public TimeSpan Active { get; init; }
    public TimeSpan Total { get; init; }
}