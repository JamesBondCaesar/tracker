

namespace Tracker.Models;

public record StatisticData
{
    public DateTimeOffset From { get; init; }
    public DateTimeOffset To { get; init; }
    public IReadOnlyCollection<StatisticItem> StatisticByTasks { get; init; } = default!;
    public IReadOnlyDictionary<DateTime, TimeSpan> TotalTimeByDays { get; init; } = default!;
}