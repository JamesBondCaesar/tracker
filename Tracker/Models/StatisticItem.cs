namespace Tracker.Models;

public record StatisticItem(DataAccess.Models.Task Task, TimeSpan TotalHours, IReadOnlyDictionary<DateTime, TimeSpan> HoursByDays);