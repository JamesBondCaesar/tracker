using Tracker.Enums;

namespace Tracker.Models;

public record ActionInfo
{
    public Guid Id { get; init; }
    public string Name { get; init; } = default!;
    public bool IsActive { get; init; }
    public DateTimeOffset CreateDate { get; init; }
    public string ProjectName { get; init; } = default!;
    public TimeSpan Active { get; init; }
    public TimeSpan Total { get; init; }
    public ActionType ActionType { get; init; }
    public IReadOnlyCollection<StatisticInfo> Statistics { get; init; } = default!;
    public ProjectInfo Project { get; init; } = default!;
}

public record StatisticInfo
{
    public DateTimeOffset StartTime { get; set; }
    public DateTimeOffset? FinishTime { get; set; }
}

public record ActionView
{
    public Guid Id { get; init; }
    public string Name { get; init; } = default!;
    public bool IsActive { get; init; }
    public DateTimeOffset CreateDate { get; init; }
    public string ProjectName { get; init; } = default!;
    public TimeSpan Active { get; init; }
    public TimeSpan Total { get; init; }
}