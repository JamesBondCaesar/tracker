namespace Tracker.Models;

public record ProjectInfo
{
    public Guid Id { get; init; }
    public string Name { get; init; } = default!;
    public TimeSpan UsedTime { get; init; }
}