using Tracker.Enums;

namespace Tracker.DataAccess.Models;

public class Task
{
    public Guid Id { get; set; }
    public string Name { get; set; } = default!;
    
    public ActionType Type { get; init; }
    public bool IsActive { get; set; }
    public DateTimeOffset CreateDate { get; set; }
    public DateTimeOffset? FinishDate { get; set; }
    public Project Project { get; set; } = default!;
    public Guid ProjectId { get; set; }
    public ICollection<Statistic>? Statistics { get; set; } = default!;
}