using Tracker.Enums;

namespace Tracker.Models;

public record StatisticItem(ActionInfo Action, TimeSpan TotalHours, ActionType ActionType, IReadOnlyDictionary<DateTime, TimeSpan> HoursByDays);