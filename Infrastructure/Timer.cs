using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Tracker.DataAccess;
using Tracker.Models;
using Task = System.Threading.Tasks.Task;

namespace Tracker.Infrastructure;

public class Timer : ITimer
{
    private readonly DatabaseContext _context;
    private readonly IMapper _mapper;

    public Timer(DatabaseContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task StartTask(string taskName, CancellationToken cancellationToken = default)
    {
        var task = await GetTask(taskName, cancellationToken);

        if (!task.IsActive)
            throw new Exception("Task is not active");

        if (task.Statistics == null || !task.Statistics.Any() || task.Statistics.All(x => x.FinishTime.HasValue))
        {
            await _context.Statistics.AddAsync(new DataAccess.Models.Statistic
            {
                Task = task,
                StartTime = DateTimeOffset.UtcNow
            }, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);
        }
    }

    public async Task StopTask(string taskName, CancellationToken cancellationToken = default)
    {
        var task = await GetTask(taskName, cancellationToken);

        if (!task.IsActive)
            throw new Exception("Task is not active");

        var activeStatistic = task.Statistics?.Where(x => !x.FinishTime.HasValue);

        if (activeStatistic == null) return;

        foreach (var activeTask in activeStatistic)
        {
            activeTask.FinishTime = DateTimeOffset.UtcNow;
        }

        await _context.SaveChangesAsync(cancellationToken);
    }

    public async Task<IReadOnlyCollection<ActionInfo>> TrackingActions(CancellationToken cancellationToken = default)
    {
        var res = (await _context.Tasks.Where(x => x.Statistics!.Any(y => !y.FinishTime.HasValue))
                .Include(x => x.Project)
                .Include(x => x.Statistics)
                .ToListAsync(cancellationToken))
            .Select(_mapper.Map<ActionInfo>)
            .ToList();

        return res;
    }

    private async Task<DataAccess.Models.Task> GetTask(string taskName, CancellationToken cancellationToken)
    {
        var task = await _context.Tasks
            .Include(x => x.Statistics)
            .FirstOrDefaultAsync(x => x.Name == taskName, cancellationToken);

        if (task == null)
            throw new Exception($"Task '{taskName}' not found");
        return task;
    }
}