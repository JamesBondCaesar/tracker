using Tracker.Models;
using Task = System.Threading.Tasks.Task;

namespace Tracker.Infrastructure;

public interface ITimer
{
    public Task StartTask(string taskName, CancellationToken cancellationToken = default);
    public Task StopTask(string taskName, CancellationToken cancellationToken = default);
  //  public Task StartMeeting(string projectName, string metingName, CancellationToken cancellationToken = default);
    //public Task StopMeeting(string projectName, string metingName, CancellationToken cancellationToken = default);
    public Task<IReadOnlyCollection<ActionInfo>> TrackingActions(CancellationToken cancellationToken = default);
}