using Tracker.Models;
using Task = System.Threading.Tasks.Task;

namespace Tracker.Infrastructure;

public interface IProjectManager
{
    public Task Create(string projectName, CancellationToken cancellationToken = default);
    public Task Delete(string projectName, CancellationToken cancellationToken = default);
    public Task<IReadOnlyCollection<ProjectInfo>> Get(CancellationToken cancellationToken = default);
}