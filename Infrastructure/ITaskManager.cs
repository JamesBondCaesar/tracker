using Tracker.Models;

namespace Tracker.Infrastructure;

public interface ITaskManager
{
    public Task CreateTask(string projectName, string taskName, CancellationToken cancellationToken = default);
    public Task Delete(string taskName, CancellationToken cancellationToken = default);

    public Task<IReadOnlyCollection<ActionInfo>> GetAll(string[]? projects,
        DateTimeOffset? from,
        DateTimeOffset? to,
        CancellationToken cancellationToken = default);

    public Task<IReadOnlyCollection<ActionInfo>> GetActive(string[]? projects,
        DateTimeOffset? from,
        DateTimeOffset? to,
        CancellationToken cancellationToken = default);

    public Task Activate(string taskName, CancellationToken cancellationToken = default);
    public Task Deactivate(string taskName, CancellationToken cancellationToken = default);
}