﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Tracker;
using Tracker.Controllers;
using Tracker.Extensions;
using Tracker.Infrastructure;
using Console = Tracker.Infrastructure.Console;

var builder = ConsoleApp.CreateBuilder(args);
builder.ConfigureServices((ctx, services) =>
{
    services.AddDatabaseContext();

    services.AddScoped<IProjectManager, ProjectManager>();
    services.AddScoped<ITaskManager, TaskManager>();
    services.AddScoped<ITimer, Tracker.Infrastructure.Timer>();
    services.AddAutoMapper(typeof(TrackerLayer).Assembly);
}).ConfigureLogging(x=>x.SetMinimumLevel(LogLevel.Warning));

var app = builder.Build();

app.AddSubCommands<ProjectsController>();
app.AddSubCommands<TaskController>();
app.AddSubCommands<StatisticController>();
app.AddCommands<TrackerController>();

app.Run();