using AutoMapper;
using Tracker.Infrastructure;
using Tracker.Models;
using Console = Tracker.Infrastructure.Console;

namespace Tracker.Controllers;

public class TrackerController : ConsoleAppBase
{
    private readonly ITimer _timer;
    private readonly IMapper _mapper;

    public TrackerController(ITimer timer, IMapper mapper)
    {
        _timer = timer;
        _mapper = mapper;
    }

    [Command("start", "Delete task")]
    public Task Start([Option(0, "Task name")] string taskName) => _timer.StartTask(taskName);

    [Command("stop", "Delete task")]
    public Task Stop([Option(0, "Task name")] string taskName) => _timer.StopTask(taskName);

    [Command("list", "List of tracking tasks")]
    public async Task List()
    {
        var tasks = await _timer.TrackingActions();
        Console.PrintTable(tasks.Select(_mapper.Map<ActionView>));
    }
}