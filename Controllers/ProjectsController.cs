using Tracker.Infrastructure;
using Tracker.Models;

namespace Tracker.Controllers;

[Command("project", "Projects managr")]
public class ProjectsController : ConsoleAppBase
{
    private readonly IProjectManager _projectManager;

    public ProjectsController(IProjectManager projectManager)
    {
        _projectManager = projectManager;
    }

    [Command("add", "Add new project")]
    public Task Log([Option(0, "Project name")] string projectName) => _projectManager.Create(projectName);

    [Command("delete", "Delete project")]
    public Task Delete([Option(0, "Project name")] string projectName) => _projectManager.Delete(projectName);

    [Command("list", "List project")]
    public async Task List([Option("n", "names")] string[]? names = null)
    {
        var projects = await _projectManager.Get();

        if (names != null && names.Any())
            projects = projects.Where(x => names.Contains(x.Name)).ToList();

        Infrastructure.Console.PrintTable(projects);
    }
}