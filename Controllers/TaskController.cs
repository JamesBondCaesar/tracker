using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Tracker.DataAccess;
using Tracker.Enums;
using Tracker.Infrastructure;
using Tracker.Models;
using Console = Tracker.Infrastructure.Console;

namespace Tracker.Controllers;

[Command("task", "Tasks managr")]
public class TaskController : ConsoleAppBase
{
    private readonly ITaskManager _taskManager;
    private readonly IMapper _mapper;

    public TaskController(ITaskManager taskManager, IMapper mapper)
    {
        _taskManager = taskManager;
        _mapper = mapper;
    }

    [Command("add", "Add new task")]
    public Task Create([Option(0, "Project name")] string projectName, [Option(1, "Task name")] string taskName) =>
        _taskManager.CreateTask(projectName, taskName);

    [Command("delete", "Delete task")]
    public Task Delete([Option(0, "Task name")] string taskName) =>
        _taskManager.Delete(taskName);

    [Command("list", "Tasks list")]
    public async Task List([Option("t", "Active / All")] TaskType type = TaskType.Active,
        [Option("p", "projects")] string[]? projects = null,
        DateTimeOffset? from = null,
        DateTimeOffset? to = null)
    {
        var tasks = type == TaskType.Active
            ? await _taskManager.GetActive(projects, from, to)
            : await _taskManager.GetAll(projects, from, to);

        Console.PrintTable(tasks.Select(_mapper.Map<ActionView>));
    }

    [Command("activate", "Activate task")]
    public Task Activate([Option(0, "Task name")] string taskName) => _taskManager.Activate(taskName);

    [Command("deactivate", "Deactivate task")]
    public Task Deactivate([Option(0, "Task name")] string taskName) => _taskManager.Deactivate(taskName);
}